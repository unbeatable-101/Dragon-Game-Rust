use rand::Rng;
use std::{io, thread, time::Duration};

fn main() {
    let mut play_again = "yes".to_string();
    while play_again.contains('y') {
        display_intro();
        let cave: u32 = choose_cave();
        enter(cave);
        println!("Do you want to play again? (Yes or No)");
        play_again = String::new();
        io::stdin()
            .read_line(&mut play_again)
            .expect("failed to read line");
        println!("{play_again}");
    }
    println!("Bye!");
}

fn display_intro() {
    println!(
        "You are in a land full of dragons. In front of you, \
        you see three caves. In one cave, the dragon is friendly \
        and will share his treasure with you. The next dragon \
        is greedy and hungry, and will eat you on sight. The final \
        dragon has no treasure and is quite scared of people, it will fly away \
        on sight."
    );
}

fn choose_cave() -> u32 {
    loop {
        let mut cave = String::new();
        println!("Which cave will you go into? (1, 2 or 3)");
        io::stdin()
            .read_line(&mut cave)
            .expect("failed to read line");
        let cave: u32 = match cave.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
        if cave == 1 || cave == 2 || cave == 3 || cave == 4 {
            return cave;
        };
    }
}

fn enter(cave: u32) {
    if cave == 4 {
        println!(
            "You look up and see a hole in the ceiling,
you climb through it and find a whole room full of tresures!"
        );
        println!();
        thread::sleep(Duration::from_secs(2));
        println!(
            "As you are bagging what you found, you see somebody \
            approaching you, \"My name is Mailliw\" He says, \"you have found the secrect room, \
            and for that you must be rewarded.\""
        );
        println!();
        thread::sleep(Duration::from_secs(2));
        println!(
            "You thought that the room was already stuffed with treasures,
but then more rain down from the ceiling!"
        );
        println!();
    } else {
        println!("You approach the cave…");
        thread::sleep(Duration::from_secs(2));
        println!("It is dark and spooky…");
        thread::sleep(Duration::from_secs(2));
        println!("A large dragon jumps out in front of you! They open their jaws and…");
        println!();
        thread::sleep(Duration::from_secs(2));
        let friendly_cave = rand::thread_rng().gen_range(1..=2);
        let mut unfriendly_cave = rand::thread_rng().gen_range(1..=2);
        while friendly_cave == unfriendly_cave {
            unfriendly_cave = rand::thread_rng().gen_range(1..=2);
        }
        if cave == unfriendly_cave {
            println!("Gobbles you down in one bite!");
        } else if cave == friendly_cave {
            println!("Gives your their treasure!");
        } else {
            println!("Fly away!");
        };
    }
}
